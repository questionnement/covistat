import france_data_management as data
import democratie.france_data_democratie as fdd

data.download_data()
data.download_data_variants_deps()

fdd.download_votes()

print("finished")
