import pandas as pd
import requests, os
from io import BytesIO
from urllib.request import urlopen
from zipfile import ZipFile

PATH = '../../data/france/democratie/'

def download_electionlegislative_2017():
    data = requests.get("https://public.opendatasoft.com/explore/dataset/elections-legislatives-2017-resultats-par-bureaux-de-vote-tour-2/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B")
    with open(PATH + "el_2017.csv", "wb") as f:
        f.write(data.content)

def download_votes():
    url = "http://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.json.zip"
    with urlopen(url) as zipresp:
        with ZipFile(BytesIO(zipresp.read())) as zfile:
            zfile.extractall(PATH + "votes/")

def import_electionlegislative_2017():
    path = os.path.abspath(PATH + 'el_2017.csv')
    df = pd.read_csv(path, sep=";")
    df = df.groupby(["Code de la circonscription", "Département", "Nom", "Prénom"]).sum()
    df.reset_index(inplace=True)
    df = df[['Nom', 'Prénom', 'Département', 'Code de la circonscription', 'Inscrits', 'Abstentions', 'Votants','Blancs', 'Nuls', 'Exprimés', 'Voix']]
    return df

def import_deputes():
    df = import_electionlegislative_2017()
    path = os.path.abspath(PATH + 'deputes.csv')
    df2 = pd.read_csv(path)
    df2.loc[:,"Nom"] = df2["Nom"].str.upper()
    df3 = df2.merge(df, how="left", left_on=["Numéro de circonscription", "Département", "Nom"], right_on=["Code de la circonscription", "Département", "Nom"], suffixes=('_deputes','_votes'))
    df4 = df3[['identifiant', 'Prénom_deputes', 'Nom', 'Région', 'Département',
       'Numéro de circonscription', 'Profession', 'Groupe politique (complet)',
       'Groupe politique (abrégé)', 'Inscrits', 'Exprimés', 'Voix']]
    df4.rename(columns={'Prénom_deputes':'Prénom'}, inplace=True)

    df4.loc[:,"%Voix/Inscrits.Sum"] = df4["Voix"] / df4["Inscrits"].sum() * 100.0
    df4.loc[:,"%Voix/Inscrits"] = df4["Voix"] / df4["Inscrits"] * 100.0
    df4.loc[:,"%Voix/Exprimés.Sum"] = df4["Voix"] / df4["Exprimés"].sum() * 100.0
    df4.loc[:,"%Voix/Exprimés"] = df4["Voix"] / df4["Exprimés"] * 100.0
    return df4