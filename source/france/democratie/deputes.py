from os import listdir
from os.path import isfile, join
from pathlib import Path
import json
PATH = "../../data/france/democratie/acteurs/acteur/"

class Depute:

    def __init__(self, id):
        path = PATH+id+".json"
        with open(path) as f:
            self._data = json.load(f)       
    
    def __str__(self):
        return "{} {}".format(self.get_lastname(),self.get_firstname())
    
    def __repr__(self):
        return "{} {}".format(self.get_lastname(),self.get_firstname())

    def get_lastname(self):
        return self._data["acteur"]["etatCivil"]["ident"]["nom"].upper()
        
    def get_firstname(self):
        return self._data["acteur"]["etatCivil"]["ident"]["prenom"]

class Deputes:
    def __init__(self):
        self.deputes = [Depute(Path(f).stem) for f in listdir(PATH) if isfile(join(PATH, f))]