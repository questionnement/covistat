from os import EX_CANTCREAT, listdir
from os.path import isfile, join
from pathlib import Path
import json
from democratie.deputes import Depute

PATH = "../../data/france/democratie/votes/json/"

class Vote:
    def __init__(self, id) -> None:
        path = PATH+id+".json"
        with open(path) as f:
            self._data = json.load(f)
    
    def __repr__(self) -> str:
        return self.get_titre()

    def get_titre(self) -> str:
        return self._data['scrutin']['titre']

    def get_syntheseVote(self) -> str:
        return self._data['scrutin']['syntheseVote']

    def get_pour(self):
        deputes = []
        for groupe in self._data['scrutin']['ventilationVotes']["organe"]["groupes"]["groupe"]:
            try:
                for votant in groupe["vote"]["decompteNominatif"]["pours"]["votant"]:
                    depute = Depute(votant["acteurRef"])
                    deputes.append(depute)
            except:
                pass
        return deputes
    
    def get_contre(self):
        deputes = []
        for groupe in self._data['scrutin']['ventilationVotes']["organe"]["groupes"]["groupe"]:
            try:
                for votant in groupe["vote"]["decompteNominatif"]["contres"]["votant"]:
                    depute = Depute(votant["acteurRef"])
                    deputes.append(depute)
            except:
                pass
        return deputes

    def get_abstention(self):
        deputes = []
        for groupe in self._data['scrutin']['ventilationVotes']["organe"]["groupes"]["groupe"]:
            try:
                for votant in groupe["vote"]["decompteNominatif"]["abstentions"]["votant"]:
                    depute = Depute(votant["acteurRef"])
                    deputes.append(depute)
            except:
                pass
        return deputes

class Votes:
    def __init__(self):
        self.votes = [Vote(Path(f).stem) for f in listdir(PATH) if isfile(join(PATH, f))]
    
    def get_syntheseVotes(self):
        synt = []
        for vote in self.votes:
            synt.append(vote.get_syntheseVote())
        return synt