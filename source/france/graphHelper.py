from datetime import datetime

def addMoreInfoInTimedGraph(fig,masque = False,holiday = False, incidence=False):
    if masque:
        fig.add_vrect(x0=datetime(2021,10,4), x1=datetime(2021,11,8), fillcolor="red", opacity=0.2, annotation_text="Retrait du masque", line_width=0)
        fig.add_vrect(x0=datetime(2021,10,11), x1=datetime(2021,11,8), fillcolor="red", opacity=0.2, line_width=0)
    if holiday:        
        fig.add_vrect(x0=datetime(2021,12,19), x1=datetime(2022,1,2), fillcolor="blue", opacity=0.2, annotation_text="Vacances de Noël", annotation_position="bottom left", line_width=0)
        fig.add_vrect(x0=datetime(2021,10,30), x1=datetime(2021,11,8), fillcolor="blue", opacity=0.2, annotation_text="Vacances de la toussaint", annotation_position="bottom left", line_width=0)
        fig.add_vrect(x0=datetime(2021,7,6), x1=datetime(2021,9,2), fillcolor="blue", opacity=0.2, annotation_text="Vacances d'été", annotation_position="bottom left", line_width=0)
        fig.add_vrect(x0=datetime(2021,5,12), x1=datetime(2021,5,17), fillcolor="blue", opacity=0.2, annotation_text="Pont de l'ascension", annotation_position="top left", line_width=0)
        fig.add_vrect(x0=datetime(2021,4,10), x1=datetime(2021,4,26), fillcolor="blue", opacity=0.2, annotation_text="Vacances de printemps", annotation_position="bottom left", line_width=0)
        fig.add_vrect(x0=datetime(2021,2,6), x1=datetime(2021,3,1), fillcolor="blue", opacity=0.2, annotation_text="Vacances d'hiver", annotation_position="top left", line_width=0)
        fig.add_vrect(x0=datetime(2020,12,19), x1=datetime(2021,1,4), fillcolor="blue", opacity=0.2, annotation_text="Vacances de Noël", annotation_position="bottom left", line_width=0)
        fig.add_vrect(x0=datetime(2020,9,17), x1=datetime(2020,10,2), fillcolor="blue", opacity=0.2, annotation_text="Vacances de la toussaint", annotation_position="top left", line_width=0)
    if incidence:
        fig.add_hline(y=50)
        fig.add_hline(y=150)
        fig.add_hline(y=250)
        fig.add_hline(y=400)